
## convert

Download and install

Add C:\Program Files\ImageMagick-7.1.0-Q16-HDRI to PATH

Be sure to add it before C:\Windows\system32 which ship another convert (to 
convert between file system types)

## pdftoppm

Download 

Add C:\Program Files\poppler-22.04.0\Library\bin to PATH

## gs

Download gs1000w64.exe

Install

Copy C:\Program Files\gs\gs10.00.0\bin\gswin64c.exe to C:\Program Files\gs\gs10.00.0\bin\gs.exe

## tesseract

Install

Add C:\Program Files\Tesseract-OCR to PATH