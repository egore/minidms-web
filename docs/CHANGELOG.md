## [1.0.21](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.20...1.0.21) (2025-02-26)


### Bug Fixes

* **deps:** update quarkus.platform.version to v3.18.2 ([f517e20](https://gitlab.com/appframework/egore-personal/minidms-web/commit/f517e20c24280059b6222425406600b268215d52))
* **deps:** update quarkus.platform.version to v3.18.3 ([69f72e5](https://gitlab.com/appframework/egore-personal/minidms-web/commit/69f72e5d9d99250318c203eb62486fb1a6c2213a))
* **deps:** update quarkus.platform.version to v3.18.4 ([cd4c795](https://gitlab.com/appframework/egore-personal/minidms-web/commit/cd4c795115a49d0873335c2a6dd9d13ba4b0435f))

## [1.0.20](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.19...1.0.20) (2025-02-04)


### Bug Fixes

* **deps:** update dependency org.webjars.npm:pdfjs-dist to v4.10.38 ([7a99081](https://gitlab.com/appframework/egore-personal/minidms-web/commit/7a99081c87d90bcea50d20d2b0e882a72b3d8112))

## [1.0.19](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.18...1.0.19) (2025-02-03)

## [1.0.18](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.17...1.0.18) (2025-02-02)


### Bug Fixes

* **deps:** update dependency org.apache.pdfbox:pdfbox to v3.0.4 ([150ffae](https://gitlab.com/appframework/egore-personal/minidms-web/commit/150ffaee690cf6308cd590c5e005b664a73fb6db))
* **deps:** update dependency org.apache.tika:tika-core to v3.1.0 ([2a10c4f](https://gitlab.com/appframework/egore-personal/minidms-web/commit/2a10c4f13735d5d08dd52ae030b55ec36bda9da0))
* **deps:** update quarkus.platform.version to v3.17.8 ([f9c1611](https://gitlab.com/appframework/egore-personal/minidms-web/commit/f9c1611527f44f46e0ed27660a29fd3e43b2c6ed))
* **deps:** update quarkus.platform.version to v3.18.1 ([928d741](https://gitlab.com/appframework/egore-personal/minidms-web/commit/928d7414ee09413bc67f82cc960d13b7d648cdaf))

## [1.0.17](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.16...1.0.17) (2025-01-22)


### Bug Fixes

* **deps:** update dependency org.assertj:assertj-core to v3.27.3 ([4036fac](https://gitlab.com/appframework/egore-personal/minidms-web/commit/4036fac3bc0573c24ae9e72447b9c61f03df494c))
* **deps:** update quarkus.platform.version to v3.17.7 ([a0b0974](https://gitlab.com/appframework/egore-personal/minidms-web/commit/a0b09740e0636e4d32adf92c5cbbc642ea210501))

## [1.0.16](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.15...1.0.16) (2025-01-15)


### Bug Fixes

* **deps:** update quarkus.platform.version to v3.17.6 ([10ec4a0](https://gitlab.com/appframework/egore-personal/minidms-web/commit/10ec4a0d53587f870c4ac01da7bd4a5db64433bd))

## [1.0.15](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.14...1.0.15) (2025-01-08)


### Bug Fixes

* **deps:** update dependency org.assertj:assertj-core to v3.27.2 ([daff56b](https://gitlab.com/appframework/egore-personal/minidms-web/commit/daff56b55365c2d1ecd23c004f928095d1434304))

## [1.0.14](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.13...1.0.14) (2024-12-28)


### Bug Fixes

* **deps:** update quarkus.platform.version to v3.17.5 ([1d8127f](https://gitlab.com/appframework/egore-personal/minidms-web/commit/1d8127fe85a17980c630990bc3bfda6f873389d3))

## [1.0.13](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.12...1.0.13) (2024-12-25)


### Bug Fixes

* **deps:** update dependency org.assertj:assertj-core to v3.27.0 ([51085fb](https://gitlab.com/appframework/egore-personal/minidms-web/commit/51085fbd8f252505956cec5e3cd00f2396c4aec7))

## [1.0.12](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.11...1.0.12) (2024-12-13)


### Bug Fixes

* **deps:** update dependency org.webjars.npm:pdfjs-dist to v4.9.155 ([fe8577e](https://gitlab.com/appframework/egore-personal/minidms-web/commit/fe8577eb7229d9f4b4e3a75a7bb321e2c16a03c3))

## [1.0.11](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.10...1.0.11) (2024-12-06)

## [1.0.10](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.9...1.0.10) (2024-12-05)

## [1.0.9](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.8...1.0.9) (2024-11-27)

## [1.0.8](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.7...1.0.8) (2024-11-20)

## [1.0.7](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.6...1.0.7) (2024-11-15)


### Bug Fixes

* **deps:** update dependency org.webjars.npm:pdfjs-dist to v4 ([63b7b8c](https://gitlab.com/appframework/egore-personal/minidms-web/commit/63b7b8c1a353bc2c761040a8ee5f61f93573a2f5))

## [1.0.6](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.5...1.0.6) (2024-11-07)

## [1.0.5](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.4...1.0.5) (2024-11-06)


### Bug Fixes

* **deps:** update dependency org.webjars.npm:pdfjs-dist to v3.11.174 ([bdb7dce](https://gitlab.com/appframework/egore-personal/minidms-web/commit/bdb7dcec6e008c65ac1bce1f10750320ec81175e))

## [1.0.4](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.3...1.0.4) (2024-11-06)

## [1.0.3](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.2...1.0.3) (2024-11-01)

## [1.0.2](https://gitlab.com/appframework/egore-personal/minidms-web/compare/1.0.1...1.0.2) (2024-10-23)


### Bug Fixes

* **deps:** update dependency org.apache.tika:tika-core to v3 ([59bb80e](https://gitlab.com/appframework/egore-personal/minidms-web/commit/59bb80e254a8ac9052227696b38f67dda0f19547))

## [1.0.1](https://gitlab.com/appframework/egore-personal/minidms-web/compare/v1.0.0...1.0.1) (2024-10-22)

# 1.0.0 (2024-10-09)


### Bug Fixes

* **deps:** update dependency org.apache.pdfbox:pdfbox to v3 ([31afd8c](https://gitlab.com/appframework/egore-personal/minidms-web/commit/31afd8c977143ab16bb67099e9ceca6ed22baa42))
* **deps:** update dependency org.apache.pdfbox:pdfbox to v3.0.3 ([b103ebf](https://gitlab.com/appframework/egore-personal/minidms-web/commit/b103ebfca7970812d0439d5f1460bf6ac7591121))
* **deps:** update dependency org.assertj:assertj-core to v3.25.3 ([a652cf4](https://gitlab.com/appframework/egore-personal/minidms-web/commit/a652cf4ffa75e5b0517c5463d3e8e0057b216341))
* **deps:** update dependency org.assertj:assertj-core to v3.26.0 ([d6e95c0](https://gitlab.com/appframework/egore-personal/minidms-web/commit/d6e95c035e83623646a1c44c7de256576ab6a4be))
* **deps:** update dependency org.assertj:assertj-core to v3.26.3 ([0804315](https://gitlab.com/appframework/egore-personal/minidms-web/commit/080431581724c52dc348f74992f61d71d737574c))
* **deps:** update dependency org.camunda.bpm:camunda-engine to v7.21.0 ([01bc7d1](https://gitlab.com/appframework/egore-personal/minidms-web/commit/01bc7d13b24e3482d70f2de0118265360a743cb5))
* **deps:** update dependency org.camunda.bpm:camunda-engine to v7.22.0 ([c7b6b48](https://gitlab.com/appframework/egore-personal/minidms-web/commit/c7b6b489f20b55889f42f161329ebc52aa724a6d))
* **deps:** update dependency org.webjars.npm:pdfjs-dist to v3.11.174 ([c602bd4](https://gitlab.com/appframework/egore-personal/minidms-web/commit/c602bd433b928303079ffe7e0165a02f636bc08c))
* **deps:** update dependency org.webjars.npm:pdfjs-dist to v4 ([fa7bd5b](https://gitlab.com/appframework/egore-personal/minidms-web/commit/fa7bd5bd040166899701e4e699d100432e632e62))
* **deps:** update dependency org.webjars.npm:pdfjs-dist to v4.1.392 ([5307324](https://gitlab.com/appframework/egore-personal/minidms-web/commit/530732448a530166979145b1d271feec4f84f68e))
* **deps:** update dependency org.webjars.npm:pdfjs-dist to v4.2.67 ([7861de8](https://gitlab.com/appframework/egore-personal/minidms-web/commit/7861de8d86e02837a69a1f6d35d3c55cff63b256))
* **deps:** update dependency org.webjars.npm:pdfjs-dist to v4.3.136 ([f7104d3](https://gitlab.com/appframework/egore-personal/minidms-web/commit/f7104d38a204b8ec0948a88a3f02e0c9ac863190))
* **deps:** update dependency org.webjars.npm:pdfjs-dist to v4.4.168 ([0254a41](https://gitlab.com/appframework/egore-personal/minidms-web/commit/0254a41e350baf5be55a708bec91b1f3deec06a0))
* **deps:** update dependency org.webjars.npm:pdfjs-dist to v4.6.82 ([8b1a849](https://gitlab.com/appframework/egore-personal/minidms-web/commit/8b1a849e8bff645a5b2db96ccdc85f95005b977d))
* **deps:** update dependency org.webjars.npm:picocss__pico to v2 ([b5a9f97](https://gitlab.com/appframework/egore-personal/minidms-web/commit/b5a9f97e12aabf462b383571314c901d117dd4a4))


### Features

* Adapt workflow for better generation ([f7198ef](https://gitlab.com/appframework/egore-personal/minidms-web/commit/f7198efb31ceb1deea4edddbcafc80f478cd7c96))
* Add basic nice-ish UI ([9ffe8e9](https://gitlab.com/appframework/egore-personal/minidms-web/commit/9ffe8e9d5bbe619f42226694ba0fc15b201ae503))
* Add non-working CRUD for categories ([25d2109](https://gitlab.com/appframework/egore-personal/minidms-web/commit/25d21093916022c7a18846262e76a3ace61d35b4))
* Add OpenID connect and roles ([4e1222d](https://gitlab.com/appframework/egore-personal/minidms-web/commit/4e1222df2b8d1dcb27f933e8161666aea4de9790))
* Add thumbnail generation ([43a615b](https://gitlab.com/appframework/egore-personal/minidms-web/commit/43a615b9dad089f5474a9c8373bb5963974b6f10))
* Convert browsing from JSON to HTML generation ([f3e911a](https://gitlab.com/appframework/egore-personal/minidms-web/commit/f3e911af3e2a11223d0f1e7254acf575e9192d68))
* Enable convert to work ([aef9be2](https://gitlab.com/appframework/egore-personal/minidms-web/commit/aef9be2a088dab661d2d4261c31c833c4d376f40))
* Implement interactive PDF rendering ([579794c](https://gitlab.com/appframework/egore-personal/minidms-web/commit/579794ce6e66ad2e1fc492b96ecbaa84a328e7e4))
* Implement thumbnail download ([0f16402](https://gitlab.com/appframework/egore-personal/minidms-web/commit/0f16402d00f88d722d12ef56ac99a3c29c22f2c9))
* Make Process also return whether alls files could be deleted ([3420ef8](https://gitlab.com/appframework/egore-personal/minidms-web/commit/3420ef83eaac60a52ddfb64d423348398eac2ab1))
* Make ProcessingVariables return whether alls files could be deleted ([1a602dd](https://gitlab.com/appframework/egore-personal/minidms-web/commit/1a602dd387d43eac228311ddda1e2cdc79fbda3e))
* Redirect to details page after upload ([2144cb9](https://gitlab.com/appframework/egore-personal/minidms-web/commit/2144cb9c724da6b6067ba8e933a7e7ccf9ae7473))
* Return stderr as well ([b56c6f0](https://gitlab.com/appframework/egore-personal/minidms-web/commit/b56c6f0eda42a8299c0dd21660179266f6fec945))
