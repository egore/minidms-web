package de.egore911.minidms.model;

import java.util.List;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.quarkus.hibernate.reactive.panache.PanacheEntity;

/**
 * Logical grouping of a document.
 * These are usually large (i.e. "invoice", "contract", etc.).
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(name = "UQ_category_name", columnNames = "name"))
public class Category extends PanacheEntity {

    /**
     * Unique name of a category
     */
    public String name;
    /**
     * Documents enlisted in this category
     */
    @JsonIgnore
    @OneToMany(mappedBy = "category")
    public List<Document> documents;

}
