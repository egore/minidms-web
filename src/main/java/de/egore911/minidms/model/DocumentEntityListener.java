package de.egore911.minidms.model;

import java.time.LocalDateTime;

import jakarta.persistence.PrePersist;

/**
 * Listener to ensure a document gets its insert timestamp set
 */
public class DocumentEntityListener {

    @PrePersist
    public void prePersist(Document document) {
        document.created = LocalDateTime.now();
    }
}
