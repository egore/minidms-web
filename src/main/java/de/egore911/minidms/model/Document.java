package de.egore911.minidms.model;

import io.quarkus.hibernate.reactive.panache.PanacheEntity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.ForeignKey;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

/*import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;*/

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * A document, the most central entity of a DMS (document management system).
 * Contains the data, as well as a PDF representation (if not present), thumbnails and detected text.
 */
@Entity
@Table(uniqueConstraints = @UniqueConstraint(name = "UQ_document_category_name", columnNames = {"category_id", "filename"}))
//@Indexed
@EntityListeners(value = DocumentEntityListener.class)
public class Document extends PanacheEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 3839574771340325644L;

    /**
     * Filename of the document (unique per category)
     */
    public String filename;
    /**
     * MIME type of the original document (e.g. "application/vnd.oasis.opendocument.text" for .odt files)
     */
    public String mimetype;
    /**
     * The size of the original document in bytes
     */
    public long size;
    /**
     * The binary data of the original
     */
    @JsonIgnore
    public byte[] original;
    /**
     * The PDF representation of the original, if the original was not a PDF
     */
    @JsonIgnore
    public byte[] pdf;
    /**
     * The text extracted from the uploaded document (incl. text recognition)
     */
    @JsonIgnore
    @Column(columnDefinition = "text")
    //@FullTextField
    public String text;
    /**
     * The category a document
     */
    @ManyToOne
    @JoinColumn(name = "category_id", foreignKey = @ForeignKey(name="FK_document_category"))
    public Category category;
    /**
     * Timestamp the document was created
     */
    public LocalDateTime created;
    /**
     * Thumbnail of the documents first (readable) page
     */
    @JsonIgnore
    public byte[] thumbnail;

    // Getters below are necessary to expose information to EL (i.e. to use them in camunda processes)

    public String getMimetype() {
        return mimetype;
    }

    public byte[] getPdf() {
        return pdf;
    }

    public String getText() {
        return text;
    }
}
