package de.egore911.minidms;

import java.util.Map;

import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class BadArgumentException extends WebApplicationException {

    private static final long serialVersionUID = 351798616799681159L;

    public BadArgumentException(String message) {
        super(Response
                .status(Response.Status.BAD_REQUEST)
                .entity(Map.of("type", BadArgumentException.class.getSimpleName(), "message", message))
                .type(MediaType.APPLICATION_JSON)
                .build());
    }

}
