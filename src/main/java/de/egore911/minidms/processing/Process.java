package de.egore911.minidms.processing;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import jakarta.inject.Singleton;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.ProcessEngineConfiguration;

import de.egore911.minidms.model.Document;

@Singleton
public class Process {

    private ProcessEngine engine;

    @PostConstruct
    public void init() {
        engine = ProcessEngineConfiguration
                .createStandaloneInMemProcessEngineConfiguration()
                .buildProcessEngine();
        // Deploy .bpnm file
        engine.getRepositoryService()
                .createDeployment()
                .addInputStream("pdfocr.bpmn", Process.class.getResourceAsStream("/pdfocr.bpmn"))
                .activateProcessDefinitionsOn(Date.from(LocalDateTime.now().plusDays(-1).atZone(ZoneId.systemDefault()).toInstant()))
                .deploy();
        engine.getRepositoryService()
                .activateProcessDefinitionByKey("pdfocr");
    }

    @PreDestroy
    public void shutdown() {
        engine.close();
    }


    public boolean process(Document document) {
        // Parse the PDF
        ProcessingVariables vars = new ProcessingVariables(this.getClass());
        engine.getRuntimeService()
                .createProcessInstanceByKey("pdfocr")
                .setVariable("vars", vars)
                .setVariable("document", document)
                .execute();
        return vars.cleanup();
    }

}
