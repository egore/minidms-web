package de.egore911.minidms.processing.imageextraction;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.MemoryCacheImageInputStream;
import javax.imageio.stream.MemoryCacheImageOutputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;

/**
 * Split single image having multiple pages into individual images (i.e. multipage tiff)
 * This uses ImageIO
 */
public class ExtractImagesFromImageJava implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

        if (vars.tempImagesExist()) {
            return;
        }

        Path imagesPath = vars.imagesPath();

        Document document = (Document) execution.getVariable("document");

        Iterator<ImageReader> readers = ImageIO.getImageReadersByMIMEType(document.mimetype);

        while (readers.hasNext()) {
            ImageReader reader = readers.next();

            MemoryCacheImageInputStream stream = new MemoryCacheImageInputStream(new ByteArrayInputStream(document.original));
            reader.setInput(stream);

            for (int i = 0; i < reader.getNumImages(true); i++) {

                // Read the current image
                BufferedImage img = reader.read(i);

                BufferedImage rgb = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics2D graphics = rgb.createGraphics();
                graphics.setColor(Color.WHITE);
                graphics.fillRect(0, 0, img.getWidth(), img.getHeight());
                graphics.drawImage(img, 0, 0, null);

                Iterator<ImageWriter> writers = ImageIO.getImageWritersByMIMEType("image/jpeg");
                while (writers.hasNext()) {
                    ImageWriter writer = writers.next();
                    ImageWriteParam param = writer.getDefaultWriteParam();
                    param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                    param.setCompressionQuality(1);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    MemoryCacheImageOutputStream outputStream = new MemoryCacheImageOutputStream(baos);
                    writer.setOutput(outputStream);
                    writer.write(null, new IIOImage(rgb, null, null), param);
                    Path page = imagesPath.resolve("page-" + i + ".jpg");
                    Files.write(page, baos.toByteArray());
                }
            }
        }

    }

}
