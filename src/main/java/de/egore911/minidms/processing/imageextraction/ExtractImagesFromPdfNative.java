package de.egore911.minidms.processing.imageextraction;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;
import static de.egore911.minidms.utils.ExecutableUtils.run;

/**
 * Render PDF into multiple images using pdftoppm from poppler
 */
public class ExtractImagesFromPdfNative implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(ExtractImagesFromPdfNative.class);

    // Remove all text from PDF
    // "gs -o ${input_file_for_images} -sDEVICE=pdfwrite -dFILTERTEXT ${input_file}"],

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        if (!executableExists("pdftoppm")) {
            return;
        }

        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

        if (vars.tempImagesExist()) {
            return;
        }

        Document document = (Document) execution.getVariable("document");

        try {
            Path tempPdf = vars.tempPdf(document);
            Path outDir = vars.imagesPath();
            LOG.info("Extracting images to {}", outDir);
            run(vars.tempDir(), "pdftoppm", "-r", "300", "-jpeg", tempPdf.toAbsolutePath().toString(), outDir.toAbsolutePath() + File.separator + "page");
        } catch (IOException | InterruptedException e) {
            LOG.warn("Failed to extract images from {}", document.id, e);
        }
    }
}
