package de.egore911.minidms.processing.imageextraction;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

/**
 * Render PDF into multiple images using pdfbox PDFRenderer
 */
public class ExtractImagesFromPdfJava implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(ExtractImagesFromPdfJava.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

        if (vars.tempImagesExist()) {
            return;
        }

        Document document = (Document) execution.getVariable("document");

        try {
            Path tempPdf = vars.tempPdf(document);
            Path outDir = vars.imagesPath();

            PDDocument pdf = Loader.loadPDF(tempPdf.toFile());
            LOG.info("Extracting images to {}", outDir);

            PDFRenderer renderer = new PDFRenderer(pdf);

            for (int i = 0; i < pdf.getNumberOfPages(); i++) {
                BufferedImage image = renderer.renderImage(i);
                ImageIO.write(image, "TIFF", new File(outDir.toAbsolutePath() + File.separator + "page-" + i + ".tif"));
            }

            pdf.close();

        } catch (IOException e) {
            LOG.warn("Failed to extract images from {}", document.id, e);
        }
    }
}
