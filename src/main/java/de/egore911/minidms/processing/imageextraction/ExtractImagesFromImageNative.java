package de.egore911.minidms.processing.imageextraction;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;
import static de.egore911.minidms.utils.ExecutableUtils.run;

/**
 * Split single image having multiple pages into individual images (i.e. multipage tiff)
 * This uses convert from imagemagick
 */
public class ExtractImagesFromImageNative implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(ExtractImagesFromImageNative.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        if (!executableExists("convert")) {
            return;
        }

        Document document = (Document) execution.getVariable("document");
        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

        if (vars.tempImagesExist()) {
            return;
        }

        try {
            Path tempPdf = vars.tempPdf(document);
            Path outDir = vars.imagesPath();
            LOG.info("Extracting images to {}", outDir);
            run(vars.tempDir(), "convert", tempPdf.toAbsolutePath().toString(), "-quality", "100", "-scene", "1", outDir.toAbsolutePath() + File.separator + "page-%03d.jpg");
        } catch (IOException | InterruptedException e) {
            LOG.warn("Failed to extract images from {}", document.id, e);
        }
    }

}
