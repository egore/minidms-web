package de.egore911.minidms.processing.imageextraction;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;
import static de.egore911.minidms.utils.ExecutableUtils.run;

/**
 * Generate thumbnail from first valid individual document image using magick from imagemagick
 */
public class GenerateThumbnailsNative implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(GenerateThumbnailsNative.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        if (!executableExists("magick")) {
            return;
        }

        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

        if (!vars.tempImagesExist()) {
            return;
        }

        Document document = (Document) execution.getVariable("document");

        try (Stream<Path> images = Files
                .find(vars.imagesPath(), 1, (filePath, fileAttr) -> fileAttr.isRegularFile())) {
            images
                    .forEach((f) -> {
                        // If a thumbnail already exists, do nothing
                        if (document.thumbnail != null) {
                            return;
                        }
                        try {
                            String s = f.getFileName().toString();
                            Path thumb = f.getParent().resolve(s.substring(0, s.lastIndexOf('.')) + "-thumb.jpg");
                            run(vars.tempDir(), "magick", f.toAbsolutePath().toString(), "-resize", "256x256", thumb.toAbsolutePath().toString());
                            document.thumbnail = Files.readAllBytes(thumb);
                            Files.delete(thumb);
                        } catch (IOException | InterruptedException e) {
                            LOG.warn("Failed to detect text on {}", f, e);
                        }
                    });
        } catch (IOException e) {
            LOG.warn("Failed to detect text on {}", document.id, e);
        }

    }

}
