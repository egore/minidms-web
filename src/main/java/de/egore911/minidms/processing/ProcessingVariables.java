package de.egore911.minidms.processing;

import de.egore911.minidms.model.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serial;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class ProcessingVariables implements Serializable {

    @Serial
    private static final long serialVersionUID = -6149180973063588074L;

    private static final Logger LOG = LoggerFactory.getLogger(ProcessingVariables.class);

    public final String prefix;

    public String langs = "deu+eng";

    private String tempDir;
    private String imagesPath;
    private String tempPdf;
    private String tempText;

    public ProcessingVariables(Class<?> originator) {
        prefix = originator.getSimpleName() + this.hashCode();
    }

    public Path imagesPath() throws IOException {
        if (imagesPath == null) {
            Path images = tempDir().resolve("images");
            imagesPath = images.toAbsolutePath().toString();
            Files.createDirectories(images);
        }
        return Path.of(imagesPath);
    }

    public Path tempDir() throws IOException {
        if (tempDir == null) {
            tempDir = Files.createTempDirectory(prefix).toAbsolutePath().toString();
        }
        return Path.of(tempDir);
    }

    public Path tempPdf(Document doc) throws IOException {
        if (tempPdf == null) {
            Path file = Files.createTempFile(tempDir(), prefix, ".pdf");
            tempPdf = file.toAbsolutePath().toString();
            Files.write(file, doc.pdf != null ? doc.pdf : doc.original);
        }
        return Path.of(tempPdf);
    }

    public Path tempText() throws IOException {
        if (tempText == null) {
            Path file = Files.createTempFile(tempDir(), prefix, "-text.pdf");
            tempText = file.toAbsolutePath().toString();
        }
        return Path.of(tempText);
    }

    public boolean cleanup() {
        boolean result = true;
        if (imagesPath != null) {
            try {
                try (Stream<Path> images = Files
                        .find(Path.of(imagesPath), 1, (filePath, fileAttr) -> fileAttr.isRegularFile())
                        .filter(f -> f.getFileName().toString().endsWith(".tif"))) {
                    images
                            .forEach((f) -> {
                                try {
                                    Files.delete(f);
                                } catch (IOException ignored) {
                                }
                            });
                }
                Files.delete(Path.of(imagesPath));
            } catch (IOException e) {
                LOG.warn("Failed to delete directory {}", imagesPath);
                result = false;
            }
        }
        if (tempPdf != null) {
            try {
                Files.delete(Path.of(tempPdf));
            } catch (IOException e) {
                LOG.warn("Failed to delete file {}", tempPdf);
                result = false;
            }
        }
        if (tempText != null) {
            try {
                Files.delete(Path.of(tempText));
            } catch (IOException e) {
                LOG.warn("Failed to delete file {}", tempText);
                result = false;
            }
        }
        if (tempDir != null) {
            try {
                Files.delete(Path.of(tempDir));
            } catch (IOException e) {
                LOG.warn("Failed to delete directory {}", tempDir);
                result = false;
            }
        }
        return result;
    }

    public boolean tempImagesExist() throws IOException {
        if (!Files.exists(imagesPath())) {
            return false;
        }
        try (Stream<Path> images = Files
                .find(imagesPath(), 1, (filePath, fileAttr) -> fileAttr.isRegularFile())) {
            return images.findAny().isPresent();
        }
    }
}
