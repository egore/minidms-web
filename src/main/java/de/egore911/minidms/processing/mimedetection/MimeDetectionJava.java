package de.egore911.minidms.processing.mimedetection;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Detect mime type using Files.probeContentType
 */
public class MimeDetectionJava implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(MimeDetectionJava.class);

    @Override
    public void execute(DelegateExecution execution) {

        Document document = (Document) execution.getVariable("document");

        // Only if the mime type is not yet specified
        if (document.mimetype != null && !document.mimetype.isEmpty()) {
            return;
        }

        // First of all use the file detection from NIO
        try {
            ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

            Path tempFile = Files.createTempFile(vars.tempDir(), "", ".tmp");
            Files.write(tempFile, document.original);
            document.mimetype = Files.probeContentType(tempFile);
            Files.delete(tempFile);
        } catch (IOException e) {
            LOG.debug("Failed to create temporary file", e);
        }

        // As a fallback try to determine it from the filename
        if (document.mimetype == null && document.filename != null && !document.filename.isEmpty()) {
            document.mimetype = URLConnection.guessContentTypeFromName(document.filename);
        }

        // XXX Maybe try more, e.g. https://www.baeldung.com/java-file-mime-type

    }

}
