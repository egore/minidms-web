package de.egore911.minidms.processing.mimedetection;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import de.egore911.minidms.utils.ExecutableUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;
import static de.egore911.minidms.utils.ExecutableUtils.run;

/**
 * Detect mime type using native file command
 */
public class MimeDetectionNative implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(MimeDetectionNative.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        if (!executableExists("file")) {
            return;
        }

        Document document = (Document) execution.getVariable("document");

        // Only if the mime type is not yet specified
        if (document.mimetype != null && !document.mimetype.isEmpty()) {
            return;
        }

        try {
            ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");
            Path tempFile = Files.createTempFile(vars.tempDir(), "", ".tmp");
            try {
                Files.write(tempFile, document.original);
                ExecutableUtils.ExecutionResult result = run(vars.tempDir(), "file", "--mime-type", tempFile.toAbsolutePath().toString());
                if (result.successful) {
                    String mimetype = result.stdout.trim();
                    mimetype = mimetype.substring(mimetype.lastIndexOf(':') + 2);
                    document.mimetype = mimetype;
                }
            } finally {
                Files.delete(tempFile);
            }
        } catch (IOException | InterruptedException e) {
            LOG.debug("Failed to determine mime type", e);
        }

    }
}
