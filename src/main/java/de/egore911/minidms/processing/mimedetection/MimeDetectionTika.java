package de.egore911.minidms.processing.mimedetection;

import de.egore911.minidms.model.Document;
import org.apache.tika.Tika;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Detect mime type using Tika.detect()
 */
public class MimeDetectionTika implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(MimeDetectionTika.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        Document document = (Document) execution.getVariable("document");

        // Only if the mime type is not yet specified
        if (document.mimetype != null && !document.mimetype.isEmpty()) {
            return;
        }

        Tika tika = new Tika();
        try {
            document.mimetype = tika.detect(new ByteArrayInputStream(document.original));
        } catch (IOException e) {
            LOG.debug("Failed to detect mime type", e);
        }

        // Replace tika internal mime types
        if (document.mimetype != null) {
            switch (document.mimetype) {
                case "application/x-tika-ooxml" ->
                        document.mimetype = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case "application/x-tika-msoffice" -> document.mimetype = "application/msword";
            }
        }

    }
}
