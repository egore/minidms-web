package de.egore911.minidms.processing.imagecleanup;

import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;
import static de.egore911.minidms.utils.ExecutableUtils.run;

/**
 * Autorotate images using tesseract
 */
public class Autorotate implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(Autorotate.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        if (!executableExists("tesseract")) {
            return;
        }

        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

        try (Stream<Path> images = Files
                .find(vars.imagesPath(), 1, (filePath, fileAttr) -> fileAttr.isRegularFile())
                .filter(f -> f.getFileName().toString().endsWith(".tif"))) {
            images
                    .forEach((f) -> {
                        try {
                            run(vars.tempDir(), "tesseract", "-l", "osd+" + vars.langs, "--psm", "0", f.toAbsolutePath().toString(), vars.imagesPath().toAbsolutePath().toString());
                            Files.delete(f);
                        } catch (IOException | InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                    });
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            Files.delete(vars.tempDir().resolve("images.osd"));
        }

    }

}
