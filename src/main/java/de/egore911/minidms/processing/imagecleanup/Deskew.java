package de.egore911.minidms.processing.imagecleanup;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;
import static de.egore911.minidms.utils.ExecutableUtils.run;

/**
 * De-Skew images using imagemagick's mogrify
 */
public class Deskew implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(Deskew.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        if (!executableExists("mogrify")) {
            return;
        }

        Document document = (Document) execution.getVariable("document");
        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");
        try {
            Path tempPdf = vars.tempPdf(document);
            run(vars.tempDir(), "mogrify", "-deskew", "40", tempPdf.toAbsolutePath().toString());
        } catch (IOException | InterruptedException e) {
            LOG.warn("Failed to detect text on {}", document.id, e);
        }
    }

}
