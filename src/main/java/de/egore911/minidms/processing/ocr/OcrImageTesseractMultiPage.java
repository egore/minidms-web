package de.egore911.minidms.processing.ocr;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Path;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;
import static de.egore911.minidms.utils.ExecutableUtils.run;

/**
 * Extract text from image using tesseract
 */
public class OcrImageTesseractMultiPage implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(OcrImageTesseractMultiPage.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        if (!executableExists("tesseract")) {
            return;
        }

        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

        if (vars.tempImagesExist()) {
            return;
        }

        Document document = (Document) execution.getVariable("document");

        try {
            Path tempPdf = vars.tempPdf(document);
            run(vars.tempDir(), "tesseract", "-l", vars.langs, "-c", "tessedit_create_pdf=1", "-c", "tessedit_create_txt=1", "-c", "tessedit_pageseg_mode=1", tempPdf.toAbsolutePath().toString(), vars.tempDir().toAbsolutePath().toString());
        } catch (IOException | InterruptedException e) {
            LOG.warn("Failed to detect text on {}", document.id, e);
        }
    }
}
