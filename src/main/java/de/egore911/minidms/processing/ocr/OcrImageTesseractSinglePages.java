package de.egore911.minidms.processing.ocr;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;
import static de.egore911.minidms.utils.ExecutableUtils.run;

/**
 * Extract text from image using tesseract
 */
public class OcrImageTesseractSinglePages implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(OcrImageTesseractSinglePages.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        if (!executableExists("tesseract")) {
            return;
        }

        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

        if (!vars.tempImagesExist()) {
            return;
        }

        Document document = (Document) execution.getVariable("document");

        try (Stream<Path> images = Files
                .find(vars.imagesPath(), 1, (filePath, fileAttr) -> fileAttr.isRegularFile())) {
            images
                .forEach((f) -> {
                    try {
                        String s = f.toAbsolutePath().toString();
                        int i = s.lastIndexOf('.');
                        if (i < 0) {
                            throw new RuntimeException("No extension found in " + s);
                        }
                        String tempDir = s.substring(0, i);
                        if (run(vars.tempDir(), "tesseract", "-l", vars.langs, "-c", "tessedit_create_pdf=1", "-c", "tessedit_pageseg_mode=1", f.toAbsolutePath().toString(), tempDir).successful) {
                            Files.delete(f);
                        }
                    } catch (IOException | InterruptedException e) {
                        LOG.warn("Failed to detect text on {}", f, e);
                    }
                });
        } catch (IOException e) {
            LOG.warn("Failed to detect text on {}", document.id, e);
        }

    }
}
