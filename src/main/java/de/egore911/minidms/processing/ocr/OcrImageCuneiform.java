package de.egore911.minidms.processing.ocr;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;

public class OcrImageCuneiform implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        if (!executableExists("cuneiform")) {
            return;
        }
        // TODO "cuneiform -l ${langs} -f hocr -o ${temp_dir + param_image_no_ext + \".hocr\"} ${image_file}"
    }
}
