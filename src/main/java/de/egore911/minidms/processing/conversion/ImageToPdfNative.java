package de.egore911.minidms.processing.conversion;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;
import static de.egore911.minidms.utils.ExecutableUtils.run;

/**
 * Convert an image to PDF.
 * Uses convert from imagemagick to perform conversion
 */
public class ImageToPdfNative implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(ImageToPdfNative.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        if (!executableExists("convert")) {
            return;
        }

        Document document = (Document) execution.getVariable("document");

        // If a previous step already created the PDF, skip this stage
        if (document.pdf != null &&  document.pdf.length > 0) {
            return;
        }

        if (!document.mimetype.equals("image/jpeg") && !document.mimetype.equals("image/png") &&
                !document.mimetype.equals("image/tiff")) {
            LOG.warn("Invalid type provided {}", document.mimetype);
            return;
        }

        try {
            ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

            Path tempFileTiff = Files.createTempFile(vars.tempDir(), vars.prefix + "-conversion", ".tif");
            try {
                Files.write(tempFileTiff, document.original);
                String source = tempFileTiff.getFileName().toString();
                Path tempFilePdf = tempFileTiff.getParent().resolve(source.substring(0, source.length() - 3) + "pdf");
                String target = tempFilePdf.getFileName().toString();
                if (!run(vars.tempDir(), "convert", source, target).successful) {
                    LOG.error("Failed to convert TIFF to PDF on {}", document.id);
                    return;
                }
                document.pdf = Files.readAllBytes(tempFilePdf);
                Files.delete(tempFilePdf);
            } finally {
                Files.delete(tempFileTiff);
            }
        } catch (IOException | InterruptedException e) {
            LOG.error("Failed to convert TIFF to PDF on {}", document.id, e);
        }
    }

}
