package de.egore911.minidms.processing.conversion;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

/**
 * No-Op: does nothing on PDFs
 */
public class PdfToPdf implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
    }

}
