package de.egore911.minidms.processing.conversion;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;
import static de.egore911.minidms.utils.ExecutableUtils.run;

/**
 * Convert .doc, .docx or .odt to .pdf using LibreOffice
 */
public class WordToPdf implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(WordToPdf.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        if (!executableExists("soffice")) {
            return;
        }

        Document document = (Document) execution.getVariable("document");

        if ("application/msword".equals(document.mimetype) ||
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document".equals(document.mimetype) ||
                "application/vnd.oasis.opendocument.text".equals(document.mimetype)) {
            try {
                ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");
                Path tempFileDoc = Files.createTempFile(vars.tempDir(), vars.prefix + "-conversion", ".doc");
                try {
                    Files.write(tempFileDoc, document.original);
                    String source = tempFileDoc.getFileName().toString();
                    Path tempFilePdf = Path.of(tempFileDoc.toAbsolutePath().toString().substring(0, tempFileDoc.toAbsolutePath().toString().length() - 3) + "pdf");
                    if (!run(vars.tempDir(), "soffice", "--headless", "--convert-to", "pdf:writer_pdf_Export", "--outdir", vars.tempDir().toAbsolutePath().toString(), source).successful) {
                        LOG.error("Failed to convert Word to PDF on {}", document.id);
                        return;
                    }
                    document.pdf = Files.readAllBytes(tempFilePdf);
                    Files.delete(tempFilePdf);
                } finally {
                    Files.delete(tempFileDoc);
                }
            } catch (IOException | InterruptedException e) {
                LOG.error("Failed to convert Word to PDF on {}", document.id, e);
            }
        }

    }
}
