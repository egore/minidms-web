package de.egore911.minidms.processing.conversion;

import de.egore911.minidms.model.Document;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.graphics.image.CCITTFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.MemoryCacheImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Iterator;

/**
 * Convert an image to PDF.
 * Uses ImageIO to perform conversion
 */
public class ImageToPdfJava implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(ImageToPdfJava.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Document document = (Document) execution.getVariable("document");

        // If a previous step already created the PDF, skip this stage
        if (document.pdf != null &&  document.pdf.length > 0) {
            return;
        }

        if (!document.mimetype.equals("image/jpeg") && !document.mimetype.equals("image/png") &&
            !document.mimetype.equals("image/tiff")) {
            LOG.warn("Invalid type provided {}", document.mimetype);
            return;
        }

        try (PDDocument pdf = new PDDocument()) {
            LOG.debug("Converting image from mime type {} to PDF", document.mimetype);
            Iterator<ImageReader> readers = ImageIO.getImageReadersByMIMEType(document.mimetype);

            while (readers.hasNext()) {
                ImageReader reader = readers.next();

                MemoryCacheImageInputStream stream = new MemoryCacheImageInputStream(new ByteArrayInputStream(document.original));
                reader.setInput(stream);

                for (int i = 0; i < reader.getNumImages(true); i++) {

                    // Read the current image
                    BufferedImage img = reader.read(i);

                    // Convert the image to black&white
                    BufferedImage binary = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
                    Graphics2D graphics = binary.createGraphics();
                    graphics.setColor(Color.WHITE);
                    graphics.fillRect(0, 0, img.getWidth(), img.getHeight());
                    graphics.drawImage(img, 0, 0, null);

                    // Create a new page on the PDF
                    PDPage page = new PDPage(new PDRectangle(0, 0, img.getWidth(), img.getWidth()));
                    pdf.addPage(page);
                    try (PDPageContentStream contentStream = new PDPageContentStream(pdf, page)) {
                        // Render the image
                        PDImageXObject image = CCITTFactory.createFromImage(pdf, binary);
                        /*PDImageXObject image = JPEGFactory.createFromImage(doc, img);
                        PDImageXObject image = LosslessFactory.createFromImage(doc, grayscale);*/
                        contentStream.drawImage(image, 0, 0);
                    }
                }
            }

            ByteArrayOutputStream output = new ByteArrayOutputStream();
            pdf.save(output);
            document.pdf = output.toByteArray();
        }

    }

}
