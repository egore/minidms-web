package de.egore911.minidms.processing.postprocess;

import de.egore911.minidms.processing.ProcessingVariables;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentCatalog;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDResources;
import org.apache.pdfbox.pdmodel.graphics.PDXObject;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class RemoveImagesFromSinglePages implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(RemoveImagesFromSinglePages.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

        if (!vars.tempImagesExist()) {
            return;
        }

        try (Stream<Path> images = Files
                .find(vars.imagesPath(), 1, (filePath, fileAttr) -> fileAttr.isRegularFile())
                .filter(f -> f.getFileName().toString().endsWith(".pdf"))) {
            images
                    .forEach((f) -> {
                                try (PDDocument pdf = Loader.loadPDF(f.toFile())) {
                                    PDDocumentCatalog catalog = pdf.getDocumentCatalog();
                                    for (PDPage page : catalog.getPages()) {
                                        PDResources resources = page.getResources();
                                        for (COSName name : resources.getXObjectNames()) {
                                            if (resources.isImageXObject(name)) {
                                                resources.put(name, (PDXObject) null);
                                            }
                                        }
                                    }

                                    pdf.save(f.toFile());
                                } catch (IOException e) {
                                    LOG.error(e.getMessage(), e);
                                }
                            });
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }

    }
}
