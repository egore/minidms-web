package de.egore911.minidms.processing.postprocess;

import de.egore911.minidms.processing.ProcessingVariables;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class MergeSinglePages implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(MergeSinglePages.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

        if (!vars.tempImagesExist()) {
            return;
        }

        PDFMergerUtility merger = new PDFMergerUtility();
        Path text = vars.tempText();
        merger.setDestinationFileName(text.toAbsolutePath().toString());

        List<Path> toDelete = new ArrayList<>();

        try (Stream<Path> images = Files
                .find(vars.imagesPath(), 1, (filePath, fileAttr) -> fileAttr.isRegularFile())
                .filter(f -> f.getFileName().toString().endsWith(".pdf"))) {
            images
                    .forEach((f) -> {
                        try {
                            merger.addSource(f.toFile());
                        } catch (FileNotFoundException e) {
                            LOG.error(e.getMessage(), e);
                        }
                        toDelete.add(f);
                    });
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }

        merger.mergeDocuments(null);

        for (Path f : toDelete) {
            try {
                Files.delete(f);
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
            }
        }

    }
}
