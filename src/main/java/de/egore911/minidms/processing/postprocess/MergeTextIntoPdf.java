package de.egore911.minidms.processing.postprocess;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;
import static de.egore911.minidms.utils.ExecutableUtils.run;

public class MergeTextIntoPdf implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(MergeTextIntoPdf.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {

        if (!executableExists("qpdf")) {
            return;
        }

        ProcessingVariables vars = (ProcessingVariables) execution.getVariable("vars");

        if (vars.tempImagesExist()) {
            return;
        }
        Document document = (Document) execution.getVariable("document");

        try {
            Path tempPdf = vars.tempPdf(document);
            Path out = Files.createTempFile(vars.tempDir(), vars.prefix, "-out.pdf");
            Path tempText = vars.tempText();

            run(vars.tempDir(), "qpdf", "--overlay", tempPdf.toAbsolutePath().toString(), "--", tempText.toAbsolutePath().toString(), out.toAbsolutePath().toString());

            document.pdf = Files.readAllBytes(out);
            Files.delete(out);
        } catch (IOException | InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }
    }
}
