package de.egore911.minidms.processing.extraction;

import de.egore911.minidms.model.Document;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Extract text from PDF using pdfbox's PDFTextStripper
 */
public class ExtractTextFromPdf implements JavaDelegate {

    private static final Logger LOG = LoggerFactory.getLogger(ExtractTextFromPdf.class);

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        Document document = (Document) execution.getVariable("document");

        byte[] binary;
        if (document.pdf != null) {
            binary = document.pdf;
        } else if (document.mimetype.equals("application/pdf")) {
            binary = document.original;
        } else {
            LOG.warn("No pdf to extract the text from");
            document.text = "";
            return;
        }

        try (PDDocument pdf = Loader.loadPDF(binary)) {
            PDFTextStripper stripper = new PDFTextStripper();
            document.text = stripper.getText(pdf).trim();
        }

    }
}
