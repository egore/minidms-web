package de.egore911.minidms.resource;

import de.egore911.minidms.BadArgumentException;
import de.egore911.minidms.model.Document;
import de.egore911.minidms.model.Roles;
import de.egore911.minidms.processing.Process;

import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.quarkus.qute.CheckedTemplate;
import io.quarkus.qute.TemplateInstance;

import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.mutiny.Uni;
import jakarta.annotation.security.RolesAllowed;
import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.reactive.RestForm;
import org.jboss.resteasy.reactive.multipart.FileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.inject.Inject;
import jakarta.ws.rs.BeanParam;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.util.Set;

@Path("/upload")
@RolesAllowed(Roles.admins)
public class UploadResource {

    private static final Logger LOG = LoggerFactory.getLogger(UploadResource.class);

    private static final Set<String> ALLOWED_MIME_TYPES = Set.of(
            "application/msword",
            "application/pdf",
            "application/vnd.oasis.opendocument.text",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "image/jpeg",
            "image/png",
            "image/tiff"
    );

    @Inject
    Process process;

    @Inject
    SecurityIdentity identity;

    @CheckedTemplate
    public static class Templates {
        public static native TemplateInstance upload(String mimetypes, SecurityIdentity identity);
    }

    @GET
    @Produces(MediaType.TEXT_HTML)
    public TemplateInstance get() {
        return Templates.upload(StringUtils.join(UploadResource.ALLOWED_MIME_TYPES, ","), identity);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @WithTransaction
    public Uni<Response> upload(@BeanParam UploadFormData data) {
        if (data.file == null) {
            LOG.debug("No file data was provided during upload");
            throw new BadArgumentException("No data provided");
        }
        if (!ALLOWED_MIME_TYPES.contains(data.file.contentType())) {
            LOG.debug("Given mime type {} was not valid", data.file.contentType());
            throw new BadArgumentException("Invalid mime type");
        }

        Uni<Document> existing1;
        if (data.category != null) {
            existing1 = Document.find("filename = ?1 and category.id = ?2", data.file.fileName(), data.category).firstResult();
        } else {
            existing1 = Document.find("filename = ?1 and category is null", data.file.fileName()).firstResult();
        }
        return existing1
                .chain((existing) -> {
                    if (existing != null) {
                        LOG.debug("Document {} was already uploaded", data.file.fileName());
                        throw new BadArgumentException("Document already uploaded");
                    }

                    Document document = new Document();
                    try {
                        document.filename = data.file.fileName();
                        document.mimetype = data.file.contentType();
                        document.original = Files.readAllBytes(data.file.uploadedFile());
                        document.size = data.file.size();
                        if (data.category != null) {
                            // FIXME: document.category = Category.findById(data.category);
                        }
                    } catch (IOException e) {
                        throw new BadArgumentException("Could not upload the document");
                    }
                    if (!process.process(document)) {
                        LOG.warn("Failed to clean up during document upload {}", document.id);
                    }
                    return document.<Document>persist().map((result) -> {
                        LOG.trace("Uploaded document {} with text {}", result.id, result.text);
                        return Response.seeOther(URI.create("/details/" + result.id)).build();
                    });
                });
    }

    public static class UploadFormData {
        @RestForm("file")
        public FileUpload file;
        @RestForm("category")
        public Integer category;
    }
}
