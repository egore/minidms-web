package de.egore911.minidms.resource;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.ZoneId;

import de.egore911.minidms.model.Roles;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

import io.smallrye.mutiny.Uni;
import org.jboss.resteasy.reactive.RestPath;

import de.egore911.minidms.model.Document;

@Path("/download")
@RolesAllowed(Roles.users)
public class DownloadResource {

    private static final String RFC1123_DATE_FORMAT_PATTERN = "EEE, dd MMM yyyy HH:mm:ss zzz";

    @GET
    @Path("/original/{documentId}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Uni<Response> downloadOriginal(@RestPath Long documentId) {
        return Document.<Document>findById(documentId)
                .map((document) -> {

                    if (document == null) {
                        return Response.status(Status.NOT_FOUND).build();
                    }

                    return Response
                            .ok(document.original)
                            .header("Content-Disposition", "attachment; filename=\"" + document.filename + "\"; size=" + document.size + "; creation-date=\"" + new SimpleDateFormat(RFC1123_DATE_FORMAT_PATTERN).format(Date.from(document.created.atZone(ZoneId.systemDefault()).toInstant())) + "\"")
                            .header("Content-Length", document.size)
                            .build();
                });
    }

    @GET
    @Path("/pdf/{documentId}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Uni<Response> downloadPdf(@RestPath Long documentId) {
        return Document.<Document>findById(documentId)
                .map((document) -> {

                    if (document == null) {
                        return Response.status(Status.NOT_FOUND).build();
                    }

                    if (document.pdf == null) {
                        return Response.noContent().build();
                    }

                    return Response
                            .ok(document.pdf)
                            .header("Content-Disposition", "attachment; filename=\"" + document.filename.substring(0, document.filename.lastIndexOf('.')) + ".pdf\"; size=" + document.pdf.length + "; creation-date=\"" + new SimpleDateFormat(RFC1123_DATE_FORMAT_PATTERN).format(Date.from(document.created.atZone(ZoneId.systemDefault()).toInstant())) + "\"")
                            .header("Content-Length", document.pdf.length)
                            .build();
                });
    }

    @GET
    @Path("/thumbnail/{documentId}")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Uni<Response> downloadThumbnail(@RestPath Long documentId) {
        return Document.<Document>findById(documentId)
                .map((document) -> {

                    if (document == null) {
                        return Response.status(Status.NOT_FOUND).build();
                    }

                    if (document.thumbnail == null) {
                        return Response.noContent().build();
                    }

                    return Response
                            .ok(document.thumbnail)
                            .header("Content-Type", "image/jpeg")
                            .header("Content-Length", document.thumbnail.length)
                            .build();
                });
    }

}
