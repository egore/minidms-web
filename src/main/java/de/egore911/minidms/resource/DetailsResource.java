package de.egore911.minidms.resource;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.model.Roles;
import io.quarkus.qute.CheckedTemplate;
import io.quarkus.qute.TemplateInstance;
import io.quarkus.security.identity.SecurityIdentity;
import io.smallrye.mutiny.Uni;

import jakarta.annotation.security.RolesAllowed;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/details")
@ApplicationScoped
@RolesAllowed(Roles.users)
public class DetailsResource {

    @Inject
    SecurityIdentity identity;

    @CheckedTemplate
    public static class Templates {
        public static native TemplateInstance details(Document document, SecurityIdentity identity);
    }

    @Path("/{documentId}")
    @GET
    @Produces(MediaType.TEXT_HTML)
    public Uni<TemplateInstance> get(@PathParam("documentId") Long documentId) {
        return Document.<Document>findById(documentId)
                .map((document) -> {
                    /*if (document == null) {
                        return Response.status(Response.Status.NOT_FOUND).build();
                    }*/

                    return Templates.details(document, identity);
                });
    }

}
