package de.egore911.minidms.resource;

import java.util.Collections;
import java.util.List;

import de.egore911.minidms.model.Roles;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.annotation.security.RolesAllowed;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;

import de.egore911.minidms.model.Category;
import de.egore911.minidms.model.Document;
import io.quarkus.qute.CheckedTemplate;
import io.quarkus.qute.TemplateInstance;
import io.smallrye.mutiny.Uni;

@Path("/browse")
@RolesAllowed(Roles.users)
public class BrowseResource {

    private static final int PAGE_SIZE = 15;

    @Inject
    SecurityIdentity identity;

    @CheckedTemplate
    public static class Templates {
        public static native TemplateInstance list(List<Document> documents, SecurityIdentity identity);
    }

    @GET
    @Path("/all")
    @Produces(MediaType.TEXT_HTML)
    public Uni<TemplateInstance> getAll(@QueryParam("page") Integer page) {
        int currentPage = page != null ? page : 0;
        return Document.<Document>findAll().page(currentPage, PAGE_SIZE).list()
                .map(documents -> Templates.list(documents, identity));
    }

    @GET
    @Path("/category/{category}")
    @Produces(MediaType.APPLICATION_JSON)
    public Uni<List<Document>> getByCategory(@PathParam("category") String name,
                                        @QueryParam("page") Integer page) {
        return Category.<Category>find("name", name).firstResult()
                .map((category) -> {
            if (category == null) {
                return Collections.emptyList();
            }
            if (category.documents.size() > PAGE_SIZE) {
                int currentPage = page != null ? page : 0;
                return category.documents.subList(currentPage * PAGE_SIZE,
                        Math.min((currentPage + 1) * PAGE_SIZE, category.documents.size()));
            }
            return category.documents;
        });
    }

}
