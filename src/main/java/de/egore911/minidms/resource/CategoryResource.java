package de.egore911.minidms.resource;

import de.egore911.minidms.model.Category;
import de.egore911.minidms.model.Roles;
import io.quarkus.hibernate.reactive.rest.data.panache.PanacheEntityResource;
import io.quarkus.rest.data.panache.MethodProperties;
import io.quarkus.rest.data.panache.ResourceProperties;
import io.smallrye.mutiny.Uni;

@ResourceProperties(rolesAllowed = Roles.users)
public interface CategoryResource extends PanacheEntityResource<Category, Long> {

    @MethodProperties(rolesAllowed = Roles.admins)
    Uni<Category> add(Category entity);

    @MethodProperties(rolesAllowed = Roles.admins)
    Uni<Category> update(Long id, Category entity);

    @MethodProperties(rolesAllowed = Roles.admins)
    Uni<Boolean> delete(Long id);

}
