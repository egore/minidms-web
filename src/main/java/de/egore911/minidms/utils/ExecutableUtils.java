package de.egore911.minidms.utils;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.MalformedInputException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExecutableUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ExecutableUtils.class);

    private static final Map<String, Path> EXECUTABLE_CACHE = new HashMap<>();

    public static class ExecutionResult {
        public final boolean successful;
        public final String stdout;
        public final String stderr;
        public ExecutionResult(boolean successful, String stdout, String stderr) {
            this.successful = successful;
            this.stdout = stdout;
            this.stderr = stderr;
        }
    }

    public static boolean executableExists(String executable) {
        String paths = System.getenv("PATH");
        String os = System.getProperty("os.name").toLowerCase();
        Path result = EXECUTABLE_CACHE.get(executable);
        if (result == null) {
            LOG.debug("Checking if executable {} exists", executable);
            if (os.contains("win")) {
                for (String path : paths.split(";")) {
                    Path dir = Path.of(path);
                    Path executablePath = dir.resolve(executable + ".exe");
                    if (Files.exists(executablePath)) {
                        result = executablePath;
                        LOG.trace("Found executable {} in path {}", executable, path);
                        EXECUTABLE_CACHE.put(executable, result);
                        return true;
                    }
                }
            } else if (os.contains("linux")) {
                for (String path : paths.split(":")) {
                    Path dir = Path.of(path);
                    Path executablePath = dir.resolve(executable);
                    if (Files.exists(executablePath)) {
                        result = executablePath;
                        LOG.trace("Found executable {} in path {}", executable, path);
                        EXECUTABLE_CACHE.put(executable, result);
                        return true;
                    }
                }
            } else {
                LOG.warn("Unsupported OS: {}", os);
            }
        }
        return result != null;
    }

    public static ExecutionResult run(Path tempDirectory, String... command) throws IOException, InterruptedException {
        String executable = command[0];
        if (!executableExists(executable)) {
            return new ExecutionResult(false, "Command " + executable + " does not exist", null);
        }
        LOG.debug("Running command {}", (Object) command);
        ProcessBuilder builder;
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            Path path = EXECUTABLE_CACHE.get(executable);

            List<String> args = new ArrayList<>();
            args.add(path.toAbsolutePath().toString());
            for (int i = 1; i < command.length; i++) {
                args.add(command[i]);
            }
            builder = new ProcessBuilder(args);
        } else {
            builder = new ProcessBuilder(command);
        }
        builder.directory(tempDirectory.toFile());
        Process p = builder.start();

        int result = p.waitFor();
        try (InputStream inputStream = p.getInputStream();
            InputStream errorStream = p.getErrorStream()) {
            String stdout = getString(result, inputStream, command);
            String stderr = getString(result, errorStream, command);
            return new ExecutionResult(result == 0, stdout, stderr);
        }
    }

    private static String getString(int result, InputStream inputStream, Object command) throws IOException {
        String str;
        try {
            try {
                str = IOUtils.toString(inputStream, StandardCharsets.UTF_8);
            } catch (MalformedInputException e) {
                // convert outputs in DOS encoding for unknown reasons
                str = IOUtils.toString(inputStream, Charset.forName("IBM437"));
            }
        } catch (MalformedInputException e) {
            LOG.error("command {} failed, could not parse output", command);
            str = "";
        }
        if (result != 0) {
            LOG.warn("Exit code: {}", result);
            if (!str.isEmpty()) {
                LOG.warn(str);
            }
        }
        return str;
    }

    public static ExecutionResult run(String... command) throws IOException, InterruptedException {
        Path tempDirectory = Files.createTempDirectory("process");
        ExecutionResult result = run(tempDirectory, command);
        Files.delete(tempDirectory);
        return result;
    }

}
