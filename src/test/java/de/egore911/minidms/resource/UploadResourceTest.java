package de.egore911.minidms.resource;

import de.egore911.minidms.utils.ExecutableUtils;
import io.quarkus.test.common.http.TestHTTPEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.net.URL;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
@TestHTTPEndpoint(UploadResource.class)
class UploadResourceTest {

    @Test
    void testUpload() {
        assumeThat(ExecutableUtils.executableExists("soffice")).isTrue();
        URL lipsumUrl = UploadResourceTest.class.getResource("/lipsum/lipsum.odt");
        assertThat(lipsumUrl).isNotNull();
        given()
                .multiPart("file", new File(lipsumUrl.getFile()), "application/vnd.oasis.opendocument.text")
                .accept(ContentType.JSON)
          .when().post()
          .then()
            .statusCode(200)
            .body("filename", is("lipsum.odt"))
            .body("mimetype", is("application/vnd.oasis.opendocument.text"))
            .body("size", is(33100));
    }

}