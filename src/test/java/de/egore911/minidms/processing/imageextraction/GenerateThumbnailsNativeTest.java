package de.egore911.minidms.processing.imageextraction;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingDocumentUtil;
import de.egore911.minidms.processing.ProcessingVariables;
import de.egore911.minidms.processing.imagecleanup.Autorotate;
import de.egore911.minidms.utils.ExecutableUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GenerateThumbnailsNativeTest {

    private static Document document;

    @BeforeAll
    public static void beforeAll() throws Exception {
        assumeThat(ExecutableUtils.executableExists("magick")).isTrue();

        document = ProcessingDocumentUtil.fromClasspath("/christophbrill/Using Ghidra on MSVC WIN32 binaries.pdf");
        document.mimetype = "application/pdf";
    }

    @Test
    void test() throws Exception {
        DelegateExecution delegateExecution = mock(DelegateExecution.class);

        when(delegateExecution.getVariable("document")).thenReturn(document);

        ProcessingVariables vars = new ProcessingVariables(this.getClass());
        try {
            when(delegateExecution.getVariable("vars")).thenReturn(vars);
            new ExtractImagesFromPdfJava().execute(delegateExecution);
            new GenerateThumbnailsNative().execute(delegateExecution);
        } finally {
            try (Stream<Path> images = Files
                    .find(vars.imagesPath(), 1, (filePath, fileAttr) -> fileAttr.isRegularFile())) {
                images
                        .forEach((f) -> {
                                    try {
                                        Files.delete(f);
                                    } catch (IOException e) {
                                        throw new RuntimeException(e);
                                    }
                                }
                        );
            }
            assertThat(vars.cleanup()).isTrue();
        }

        assertThat(document.thumbnail).isNotNull();
    }

}
