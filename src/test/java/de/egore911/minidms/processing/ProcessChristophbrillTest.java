package de.egore911.minidms.processing;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.utils.ExecutableUtils;
import io.quarkus.test.junit.QuarkusTest;

import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;

import jakarta.inject.Inject;

import static de.egore911.minidms.utils.ExecutableUtils.run;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class ProcessChristophbrillTest {

    private static byte[] tiffBytes;
    private static byte[] scannedBytes;

    private static Path tempDirectory;
    private static Path orig;
    private static Path tiff;
    private static Path scanned;

    @Inject
    Process process;

    @BeforeAll
    public static void beforeAll() throws IOException, InterruptedException {

        // Given: The whole tests only works when we can convert files using imagemagick
        Assumptions.assumeTrue(ExecutableUtils.executableExists("convert"));

        // Given: The test document exists
        URL resource = ProcessChristophbrillTest.class.getResource("/christophbrill/Using Ghidra on MSVC WIN32 binaries.pdf");
        assertThat(resource).isNotNull();

        tempDirectory = Files.createTempDirectory("ProcessChristophbrillTest");
        orig = Files.createTempFile(tempDirectory, "orig", ".pdf");
        Files.write(orig, IOUtils.toByteArray(resource));
        String s = orig.getFileName().toString();

        // Given: A PDF converted to a TIFF
        tiff = orig.getParent().resolve(s.substring(0, s.lastIndexOf('.')) + ".tiff");
        {
            ExecutableUtils.ExecutionResult result = run("gs", "-o", tiff.toAbsolutePath().toString(), "-sDEVICE=tiffg4", "-r720x720", "-sPAPERSIZE=a4", orig.toAbsolutePath().toString());
            assertThat(result.successful).as(result.stdout + result.stderr).isTrue();
            tiffBytes = Files.readAllBytes(tiff);
        }

        // Given: The TIFF re-converted to a PDF
        scanned = tiff.getParent().resolve(s.substring(0, s.lastIndexOf('.')) + "-scanned.pdf");
        {
            ExecutableUtils.ExecutionResult result = run("convert", tiff.toAbsolutePath().toString(), scanned.toAbsolutePath().toString());
            assertThat(result.successful).as(result.stdout + result.stderr).isTrue();
            scannedBytes = Files.readAllBytes(scanned);
        }
    }

    @AfterAll
    public static void afterAll() throws IOException {
        if (orig != null) Files.delete(orig);
        if (tiff != null) Files.delete(tiff);
        if (scanned != null) Files.delete(scanned);
        if (tempDirectory != null) Files.delete(tempDirectory);
    }

    @Test
    void testScanned() {
        Document document = ProcessingDocumentUtil.fromByteArray("Using Ghidra on MSVC WIN32 binaries-scanned.pdf", scannedBytes);

        assertThat(process.process(document)).isTrue();

        assertThat(document.mimetype).isEqualTo("application/pdf");
        assertThat(document.text)
                .isNotNull()
                .startsWith("Using Ghidra on MSVC WIN32 binaries");
    }

    @Test
    void testTiff() {
        Document document = ProcessingDocumentUtil.fromByteArray("Using Ghidra on MSVC WIN32 binaries.tiff", tiffBytes);

        assertThat(process.process(document)).isTrue();

        assertThat(document.mimetype).isEqualTo("image/tiff");
        assertThat(document.text)
                .isNotNull()
                .startsWith("Using Ghidra on MSVC WIN32 binaries");
    }

}
