package de.egore911.minidms.processing.imagecleanup;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessLipsumTest;
import de.egore911.minidms.processing.ProcessingDocumentUtil;
import de.egore911.minidms.processing.ProcessingVariables;
import de.egore911.minidms.processing.conversion.WordToPdf;
import de.egore911.minidms.processing.imageextraction.ExtractImagesFromPdfJava;
import de.egore911.minidms.utils.ExecutableUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static de.egore911.minidms.processing.ProcessLipsumTest.convertToImage;
import static de.egore911.minidms.utils.ExecutableUtils.run;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AutorotateTest {

    private static Document document;

    @BeforeAll
    public static void beforeAll() throws Exception {
        assumeThat(ExecutableUtils.executableExists("executableExists")).isTrue();
        document = bootstrap();
    }

    static Document bootstrap() throws Exception {
        Document pdf = null;
        {
            Document document = ProcessingDocumentUtil.fromClasspath("/lipsum/lipsum.odt");
            document.mimetype = "application/vnd.oasis.opendocument.text";
            ProcessingVariables vars = new ProcessingVariables(AutorotateTest.class);
            try {
                DelegateExecution delegateExecution = mock(DelegateExecution.class);
                when(delegateExecution.getVariable("document")).thenReturn(document);
                when(delegateExecution.getVariable("vars")).thenReturn(vars);
                new WordToPdf().execute(delegateExecution);
                if (document.pdf != null) {
                    pdf = ProcessingDocumentUtil.fromByteArray("lipsum.pdf", document.pdf);
                }
            } finally {
                assertThat(vars.cleanup()).isTrue();
            }
        }

        Document tiff = convertToImage(pdf, "lipsum.tif");

        ProcessingVariables vars = new ProcessingVariables(AutorotateTest.class);
        try {
            Path tmpTiff = vars.tempPdf(tiff);
            Path scanned = tmpTiff.getParent().resolve("lipsum-scanned.pdf");
            try {
                {
                    ExecutableUtils.ExecutionResult r = run("convert", tmpTiff.toAbsolutePath().toString(), scanned.toAbsolutePath().toString());
                    assertThat(r.successful).as(r.stdout + r.stderr).isTrue();
                    document = ProcessingDocumentUtil.fromByteArray("lipsum-scanned.pdf", Files.readAllBytes(scanned));
                }
            } finally {
                Files.delete(scanned);
            }
        } finally {
            assertThat(vars.cleanup()).isTrue();
        }

        return document;
    }

    @Test
    void test() throws Exception {
        DelegateExecution delegateExecution = mock(DelegateExecution.class);

        when(delegateExecution.getVariable("document")).thenReturn(document);

        ProcessingVariables vars = new ProcessingVariables(this.getClass());
        try {
            when(delegateExecution.getVariable("vars")).thenReturn(vars);
            new ExtractImagesFromPdfJava().execute(delegateExecution);
            new Autorotate().execute(delegateExecution);
        } finally {
            assertThat(vars.cleanup()).isTrue();
        }
    }

}
