package de.egore911.minidms.processing.imagecleanup;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessLipsumTest;
import de.egore911.minidms.processing.ProcessingDocumentUtil;
import de.egore911.minidms.processing.ProcessingVariables;

import de.egore911.minidms.processing.conversion.WordToPdf;
import de.egore911.minidms.utils.ExecutableUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;

import static de.egore911.minidms.processing.ProcessLipsumTest.convertToImage;
import static de.egore911.minidms.utils.ExecutableUtils.run;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DeskewTest {

    private static Document document;

    @BeforeAll
    public static void beforeAll() throws Exception {
        assumeThat(ExecutableUtils.executableExists("executableExists")).isTrue();
        document = AutorotateTest.bootstrap();
    }

    @Test
    void test() throws Exception {
        Deskew deskew = new Deskew();
        DelegateExecution delegateExecution = mock(DelegateExecution.class);

        document.mimetype = "application/pdf";
        when(delegateExecution.getVariable("document")).thenReturn(document);

        ProcessingVariables vars = new ProcessingVariables(this.getClass());
        try {
            when(delegateExecution.getVariable("vars")).thenReturn(vars);
            deskew.execute(delegateExecution);
        } finally {
            assertThat(vars.cleanup()).isTrue();
        }
    }

}
