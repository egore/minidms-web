package de.egore911.minidms.processing.mimedetection;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingDocumentUtil;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class AbstractMimeDetection {

    protected abstract JavaDelegate getMimeDetection();

    private static Stream<Arguments> filesAndMimeTypes() {
        return Stream.of(
                Arguments.of("/empty/empty.doc", "application/msword"),
                Arguments.of("/empty/empty.docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"),
                Arguments.of("/empty/empty.odt", "application/vnd.oasis.opendocument.text"),
                Arguments.of("/empty/empty.tiff", "image/tiff"),
                Arguments.of("/empty/empty.jpg", "image/jpeg"),
                Arguments.of("/empty/empty.png", "image/png")
        );
    }

    @ParameterizedTest
    @MethodSource("filesAndMimeTypes")
    public void test(String url, String mimetype) throws Exception {
        JavaDelegate mimeDetection = getMimeDetection();
        Document document = ProcessingDocumentUtil.fromClasspath(url);

        ProcessingVariables vars = new ProcessingVariables(this.getClass());
        try {
            DelegateExecution delegateExecution = mock(DelegateExecution.class);
            when(delegateExecution.getVariable("document")).thenReturn(document);
            when(delegateExecution.getVariable("vars")).thenReturn(vars);
            mimeDetection.execute(delegateExecution);

            assertThat(document.mimetype).isNotNull();
            assertThat(document.mimetype).isEqualTo(mimetype);
        } finally {
            assertThat(vars.cleanup()).isTrue();
        }
    }

}
