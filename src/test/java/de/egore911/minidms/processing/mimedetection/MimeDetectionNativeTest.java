package de.egore911.minidms.processing.mimedetection;

import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.junit.jupiter.api.Assumptions;
import org.junit.jupiter.api.BeforeAll;

import static de.egore911.minidms.utils.ExecutableUtils.executableExists;

public class MimeDetectionNativeTest extends AbstractMimeDetection {

    @BeforeAll
    public static void beforeAll() {
        Assumptions.assumeTrue(executableExists("file"));
    }

    @Override
    protected JavaDelegate getMimeDetection() {
        return new MimeDetectionNative();
    }

}
