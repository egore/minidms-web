package de.egore911.minidms.processing.mimedetection;

import org.camunda.bpm.engine.delegate.JavaDelegate;

public class MimeDetectionTikaTest extends AbstractMimeDetection {

    @Override
    protected JavaDelegate getMimeDetection() {
        return new MimeDetectionTika();
    }

}
