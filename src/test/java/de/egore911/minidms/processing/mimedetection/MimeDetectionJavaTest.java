package de.egore911.minidms.processing.mimedetection;

import org.camunda.bpm.engine.delegate.JavaDelegate;

public class MimeDetectionJavaTest extends AbstractMimeDetection {

    @Override
    protected JavaDelegate getMimeDetection() {
        return new MimeDetectionJava();
    }

}
