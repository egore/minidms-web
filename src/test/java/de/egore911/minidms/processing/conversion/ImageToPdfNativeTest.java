package de.egore911.minidms.processing.conversion;

import de.egore911.minidms.utils.ExecutableUtils;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.junit.jupiter.api.BeforeAll;

import static org.assertj.core.api.Assumptions.assumeThat;

public class ImageToPdfNativeTest extends AbstractImageToPdfTest {

    final ImageToPdfNative processing = new ImageToPdfNative();

    @BeforeAll
    public static void beforeAll() {
        assumeThat(ExecutableUtils.executableExists("convert")).isTrue();
    }

    @Override
    public JavaDelegate getProcessing() {
        return processing;
    }
}
