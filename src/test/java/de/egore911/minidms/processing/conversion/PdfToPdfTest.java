package de.egore911.minidms.processing.conversion;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PdfToPdfTest {

    final PdfToPdf processing = new PdfToPdf();

    @Test
    public void testProcessPdf() throws Exception {
        Document document = new Document();
        document.original = new byte[] {0,1,2,3,4,5,6,7,8,9};
        document.size = document.original.length;
        document.mimetype = "application/pdf";

        DelegateExecution delegateExecution = mock(DelegateExecution.class);
        when(delegateExecution.getVariable("document")).thenReturn(document);
        ProcessingVariables vars = new ProcessingVariables(this.getClass());
        when(delegateExecution.getVariable("vars")).thenReturn(vars);
        processing.execute(delegateExecution);

        assertThat(document.pdf).isNull();
    }

    @Test
    public void testProcessTiff() throws Exception {
        Document document = new Document();
        document.original = new byte[] {0,1,2,3,4,5,6,7,8,9};
        document.size = document.original.length;
        document.mimetype = "image/tiff";

        DelegateExecution delegateExecution = mock(DelegateExecution.class);
        when(delegateExecution.getVariable("document")).thenReturn(document);
        processing.execute(delegateExecution);

        assertThat(document.pdf).isNull();
    }
}
