package de.egore911.minidms.processing.conversion;

import org.camunda.bpm.engine.delegate.JavaDelegate;

public class ImageToPdfJavaTest extends AbstractImageToPdfTest {

    final ImageToPdfJava processing = new ImageToPdfJava();

    @Override
    public JavaDelegate getProcessing() {
        return processing;
    }

}
