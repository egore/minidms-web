package de.egore911.minidms.processing.conversion;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.ProcessingDocumentUtil;
import de.egore911.minidms.processing.ProcessingVariables;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class AbstractImageToPdfTest {

    protected abstract JavaDelegate getProcessing();

    @Test
    public void testProcessPdf() throws Exception {
        Document document = ProcessingDocumentUtil.fromClasspath("/empty/empty.pdf");
        document.mimetype = "application/pdf";

        ProcessingVariables vars = new ProcessingVariables(this.getClass());
        try {
            DelegateExecution delegateExecution = mock(DelegateExecution.class);
            when(delegateExecution.getVariable("document")).thenReturn(document);
            when(delegateExecution.getVariable("vars")).thenReturn(vars);
            getProcessing().execute(delegateExecution);

            assertThat(document.pdf).isNull();
        } finally {
            assertThat(vars.cleanup()).isTrue();
        }
    }

    private static Stream<Arguments> images() {
        return Stream.of(
                Arguments.of("/empty/empty.tiff", "image/tiff"),
                Arguments.of("/empty/empty.jpg", "image/jpeg"),
                Arguments.of("/empty/empty.png", "image/png")
        );
    }

    @ParameterizedTest
    @MethodSource("images")
    public void testProcessImages(String url, String mimetype) throws Exception {
        Document document = ProcessingDocumentUtil.fromClasspath(url);
        document.mimetype = mimetype;

        ProcessingVariables vars = new ProcessingVariables(this.getClass());
        try {
            DelegateExecution delegateExecution = mock(DelegateExecution.class);
            when(delegateExecution.getVariable("document")).thenReturn(document);
            when(delegateExecution.getVariable("vars")).thenReturn(vars);
            getProcessing().execute(delegateExecution);

            assertThat(document.pdf).isNotNull();
        } finally {
            assertThat(vars.cleanup()).isTrue();
        }
    }

}
