package de.egore911.minidms.processing;

import de.egore911.minidms.model.Document;
import de.egore911.minidms.processing.conversion.WordToPdf;
import de.egore911.minidms.utils.ExecutableUtils;
import io.quarkus.test.junit.QuarkusTest;

import org.apache.commons.io.IOUtils;
import org.apache.commons.text.similarity.JaroWinklerSimilarity;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import jakarta.inject.Inject;

import static de.egore911.minidms.utils.ExecutableUtils.run;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assumptions.assumeThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@QuarkusTest
public class ProcessLipsumTest {

    private static String text;

    @Inject
    Process process;

    @BeforeAll
    public static void beforeAll() throws IOException {
        assumeThat(ExecutableUtils.executableExists("soffice")).isTrue();

        InputStream lipsumTxt = ProcessLipsumTest.class.getResourceAsStream("/lipsum/lipsum.txt");
        assertThat(lipsumTxt).isNotNull();
        text = IOUtils.toString(lipsumTxt, StandardCharsets.US_ASCII);
    }

    private static Stream<Arguments> filesAndMimeTypes() throws Exception {

        List<Arguments> result = new ArrayList<>();

        // ODT
        URL source = ProcessingDocumentUtil.class.getResource("/lipsum/lipsum.odt");
        assertThat(source).isNotNull();
        result.add(Arguments.of(ProcessingDocumentUtil.fromByteArray("lipsum.odt", IOUtils.toByteArray(source)), "application/vnd.oasis.opendocument.text", 1.0d));

        // TODO not yet supported: Plain text
        // result.add(Arguments.of(ProcessingDocumentUtil.fromClasspath("/lipsum/lipsum.txt"), "text/plain", 1.0d));

        // DOC
        {
            Path tempDir = Files.createTempDirectory(ProcessLipsumTest.class.getSimpleName());
            try {
                ExecutableUtils.ExecutionResult executionResult = run(tempDir, "soffice", "--headless", "--convert-to", "doc", "--outdir", tempDir.toAbsolutePath().toString(), Path.of(source.toURI()).toAbsolutePath().toString());
                if (executionResult.successful) {
                    Path file = tempDir.resolve("lipsum.doc");
                    try {
                        result.add(Arguments.of(ProcessingDocumentUtil.fromByteArray("lipsum.doc", Files.readAllBytes(file)), "application/msword", 1.0d));
                    } finally {
                        Files.delete(file);
                    }
                } else {
                    throw new AssertionError(executionResult.stdout);
                }
            } finally {
                Files.delete(tempDir);
            }
        }

        // DOCX
        {
            Path tempDir = Files.createTempDirectory(ProcessLipsumTest.class.getSimpleName());
            try {
                ExecutableUtils.ExecutionResult executionResult = run(tempDir, "soffice", "--headless", "--convert-to", "docx", "--outdir", tempDir.toAbsolutePath().toString(), Path.of(source.toURI()).toAbsolutePath().toString());
                if (executionResult.successful) {
                    Path file = tempDir.resolve("lipsum.docx");
                    try {
                        result.add(Arguments.of(ProcessingDocumentUtil.fromByteArray("lipsum.docx", Files.readAllBytes(file)), "application/vnd.openxmlformats-officedocument.wordprocessingml.document", 1.0d));
                    } finally {
                        Files.delete(file);
                    }
                } else {
                    throw new AssertionError(executionResult.stdout);
                }
            } finally {
                Files.delete(tempDir);
            }
        }

        // PDF
        Document pdf = null;
        {
            Document document = ProcessingDocumentUtil.fromClasspath("/lipsum/lipsum.odt");
            document.mimetype = "application/vnd.oasis.opendocument.text";
            ProcessingVariables vars = new ProcessingVariables(ProcessLipsumTest.class);
            try {
                DelegateExecution delegateExecution = mock(DelegateExecution.class);
                when(delegateExecution.getVariable("document")).thenReturn(document);
                when(delegateExecution.getVariable("vars")).thenReturn(vars);
                new WordToPdf().execute(delegateExecution);
                if (document.pdf != null) {
                    pdf = ProcessingDocumentUtil.fromByteArray("lipsum.pdf", document.pdf);
                    result.add(Arguments.of(pdf, "application/pdf", 1.0d));
                }
            } finally {
                assertThat(vars.cleanup()).isTrue();
            }
        }

        // TIFF
        Document tiff = convertToImage(pdf, "lipsum.tif");
        result.add(Arguments.of(tiff, "image/tiff", 0.90d));

        // JPEG
        result.add(Arguments.of(convertToImage(pdf, "lipsum.jpg"), "image/jpeg", 0.87d));

        // PNG
        result.add(Arguments.of(convertToImage(pdf, "lipsum.png"), "image/png", 0.9d));

        // "scanned" PDF
        ProcessingVariables vars = new ProcessingVariables(ProcessLipsumTest.class);
        try {
            Path tmpTiff = vars.tempPdf(tiff);
            Path scanned = tmpTiff.getParent().resolve("lipsum-scanned.pdf");
            try {
                {
                    ExecutableUtils.ExecutionResult r = run("convert", tmpTiff.toAbsolutePath().toString(), scanned.toAbsolutePath().toString());
                    assertThat(r.successful).as(r.stdout + r.stderr).isTrue();
                    result.add(Arguments.of(ProcessingDocumentUtil.fromByteArray("lipsum-scanned.pdf", Files.readAllBytes(scanned)), "application/pdf", 0.89d));
                }
            } finally {
                Files.delete(scanned);
            }
        } finally {
            assertThat(vars.cleanup()).isTrue();
        }
        return result.stream();
    }

    public static Document convertToImage(Document pdf, String other) throws IOException, InterruptedException {
        ProcessingVariables vars = new ProcessingVariables(ProcessLipsumTest.class);
        try {
            Path orig = vars.tempPdf(pdf);
            Path tiff = orig.getParent().resolve(other);
            try {
                ExecutableUtils.ExecutionResult r = run("convert", orig.toAbsolutePath().toString(), tiff.toAbsolutePath().toString());
                assertThat(r.successful).as(r.stdout + r.stderr).isTrue();
                return ProcessingDocumentUtil.fromByteArray(other, Files.readAllBytes(tiff));
            } finally {
                Files.delete(tiff);
            }
        } finally {
            assertThat(vars.cleanup()).isTrue();
        }
    }

    @ParameterizedTest
    @MethodSource("filesAndMimeTypes")
    void testDocument(Document document, String mimetype, Double confidence) {
        assertThat(process.process(document)).isTrue();

        assertThat(document.mimetype).isEqualTo(mimetype);

        assertThat(document.text)
                .isNotNull()
                .isNotEmpty();

        JaroWinklerSimilarity similarity = new JaroWinklerSimilarity();
        assertThat(similarity.apply(text.replaceAll("[\r\n ]", ""), document.text.replaceAll("[\r\n ]", ""))).isGreaterThanOrEqualTo(confidence);
    }
}
