package de.egore911.minidms.processing;

import de.egore911.minidms.model.Document;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.net.URL;

public class ProcessingDocumentUtil {

    public static Document fromClasspath(String url) throws IOException {
        URL resource = ProcessingDocumentUtil.class.getResource(url);
        if (resource == null) {
            throw new RuntimeException("The URL passed is not available on the classpath");
        }
        return fromByteArray(url.substring(url.lastIndexOf('/') + 1), IOUtils.toByteArray(resource));
    }

    public static Document fromByteArray(String filename, byte[] data) {
        Document doc = new Document();
        doc.original = data;
        doc.size = doc.original.length;
        doc.filename = filename;
        return doc;
    }

}
